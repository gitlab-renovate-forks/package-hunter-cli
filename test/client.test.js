/* eslint-env mocha */
'use strict'

const assert = require('assert')
const nock = require('nock')
const sinon = require('sinon')
const fs = require('fs')

const PackageHunterClient = require('../src/client')
const { TimeoutError } = require('got')
describe('Client Test', () => {
  describe('_sessionCookie Test', () => {
    const cookieName = 'AWSALBCORS'
    const cookieValue = 'sessionCookie'

    const host = 'https://example.com'

    const opts = {
      cookieName
    }

    let client
    beforeEach(() => {
      client = new PackageHunterClient(host, opts)
    })

    it('can retrieve the session cookie from an array of cookie headers in the response', () => {
      const cookies = [
        null,
        'some-header=implementation',
        `${cookieName}=${cookieValue}`
      ]

      const resp = {
        headers: {
          'set-cookie': cookies
        }
      }

      const sessionCookie = client._sessionCookie(resp)

      assert.strictEqual(sessionCookie.key, cookieName)
      assert.strictEqual(sessionCookie.value, cookieValue)
    })

    it('can retrieve the session cookie if it is the only cookie in the response', () => {
      const resp = {
        headers: {
          'set-cookie': `${cookieName}=${cookieValue}`
        }
      }

      const sessionCookie = client._sessionCookie(resp)

      assert.strictEqual(sessionCookie.key, cookieName)
      assert.strictEqual(sessionCookie.value, cookieValue)
    })
  })

  describe('_submit Test', () => {
    it('times out if API is not responsive', async () => {
      sinon.stub(fs, 'createReadStream').returns(Buffer.from('foo'))

      const host = 'https://example.com'
      const path = '/path/to/endpoint'

      nock(host)
        .post(path)
        .delayConnection(500) // delay the connection for 500ms
        .reply(500, 'Ups')

      const timeout = {
        lookup: 100,
        connect: 100,
        secureConnect: 100,
        socket: 100,
        send: 100,
        response: 100
      }
      const client = new PackageHunterClient(host, { httpOpts: { timeout } })
      await assert.rejects(
        client._submit(path, 'path/to/sources'),
        TimeoutError
      )
    })
  })
})
